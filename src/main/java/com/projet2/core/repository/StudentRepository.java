package com.projet2.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projet2.core.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Long>{

}