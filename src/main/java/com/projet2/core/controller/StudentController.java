package com.projet2.core.controller;

import com.projet2.core.entity.Student;
import com.projet2.core.service.StudentService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class StudentController {

    private StudentService studentService;

    public StudentController(StudentService studentService) {
        super();
        this.studentService = studentService;
    }

    @GetMapping("/students")
    public String listStudents(Model model) {
        model.addAttribute("students", studentService.getAllStudents());
        return "students";
    }

    @GetMapping("/students/new")
    public String createStudentForm(Model model) {
        //on crée un objet student
        Student student = new Student();
        model.addAttribute("student", student);
        return "create_student";
    }

    @PostMapping("/students")
    public String saveStudent(@ModelAttribute("student") Student student) {
        //on valide les données du formulaire
        studentService.saveStudent(student);
        return "redirect:/students";
    }

    @GetMapping("/students/edit/{id}")
    public String editStudentFrom(@PathVariable Long id, Model model) {
        //doit dans ce formulaire de modifacation utiliser l'id pour récupérer les données du student
        model.addAttribute("student", studentService.getStudentById(id));
        return "edit_student";
    }

    @PostMapping("/students/{id}")
    public String updateStudent(@PathVariable Long id, @ModelAttribute("student") Student student, Model model){
     //mettre à jour les données 
     //on récupère les données de l'étudiant conserné et on les met à jour
     Student existingStudent = studentService.getStudentById(id);
     existingStudent.setId(id);
     existingStudent.setFirstName(student.getFirstName());
     existingStudent.setLastName(student.getLastName());
     existingStudent.setEmail(student.getEmail());

     //save updted student

    studentService.updateStudent(existingStudent);

    return "redirect:/students";


    }

    //method to handle delete student

    @GetMapping("/students/{id}")
    public String deleteStudent(@PathVariable Long id) {
        studentService.deleteStudentById(id);      
        return "redirect:/students";
    }
    
}
